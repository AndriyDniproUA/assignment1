package org.example.assignment1;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class GoogleNewsAPI implements NewsAPI{
    @Override
    public String getNews() {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://google-news.p.rapidapi.com/v1/source_search?source=nytimes.com&lang=en&country=US"))
                .header("x-rapidapi-key", "918610aa8cmshb67aad08a83ca1dp1c30b8jsn8e273d47166e")
                .header("x-rapidapi-host", "google-news.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
