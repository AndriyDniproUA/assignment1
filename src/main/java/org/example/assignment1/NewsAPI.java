package org.example.assignment1;

import java.net.http.HttpResponse;

public interface NewsAPI {
    String getNews();
}
